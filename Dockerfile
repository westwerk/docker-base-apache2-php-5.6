FROM westwerk/base-apache2
MAINTAINER Michael Ruettgers <mr@westwerk.ac>

# Define packages to be installed
ENV PACKAGES \
  libapache2-mod-php5.6 \
  php5.6-cli \
  php5.6-common \
  php5.6-bcmath \
  php5.6-bz2 \
  php5.6-curl \
  php5.6-gd \
  php5.6-intl \
  php5.6-json \
  php5.6-mbstring \
  php5.6-mcrypt \
  php5.6-mysql \
  php5.6-opcache \
  php5.6-xml \
  php5.6-xsl \
  php5.6-zip \
  libxrender1 \
  libfontconfig1

# Install packages && cleanup
RUN apt-get update && \
  apt-get install -y software-properties-common && \
  LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php && \
  apt-get update && \
  apt-get upgrade -y && \
  apt-get -y install $PACKAGES && \
  apt-get remove --purge -y software-properties-common && \
  apt-get autoremove -y

EXPOSE 80
EXPOSE 443

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
